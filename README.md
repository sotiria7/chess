# README #

In order to run this program please do the following:

1. Open command line.
2. give command: cd Chess
3. give command: gradle build
4. give the command: java -jar build/libs/Example1-1.0.jar

#How to play the game

Enter the starting position coordinates of the knight in "Coordinate1" field.
Enter the ending position coordinates of the knight in "Coordinate2" field.

Push "Find Path" button.

The solution will be displayed:
	A) In the "Solution" field to the bottom of the window.
	B) Graphically: Starting point will be displayed colored in red, Ending point will be colored in green, intermediate steps will be colored in cyan.

If you wish to replay press "Reset" button.

#######