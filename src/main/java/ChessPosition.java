import java.util.Objects;

public class ChessPosition {

    public int x;
    public int y;
    public String value;
    public static int CHESS_SIZE = 8;


    public ChessPosition(String Value) throws IllegalArgumentException
    {
        //check if position is right given coordinates
        if (Value.length() == 2)
        {
            try {

                x = getNumber(Value.substring(0,1).toUpperCase());
                y = Integer.parseInt(Value.substring(1));

                if(isValid(x, y)){

                    value= Value.toUpperCase();

                }else{

                    throw new IllegalArgumentException("Invalid position coordinates");

                }
            } catch (IllegalArgumentException e) {

                throw new IllegalArgumentException("Invalid position coordinates");
            }
        } else {

            throw new IllegalArgumentException("Invalid position coordinates");
        }
    }

    public static String getAlpha(int number) {

        //Convert int to string using base 26
        String result = "";
        while (number > 0) {
            number--;
            int remainder = number % 26;
            char digit = (char) (remainder + 97);
            result = digit + result;
            number = (number - remainder) / 26;
        }

        return result;
    }

    public static int getNumber(String str) {

        //Convert string to int using base 26
        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        if(str.length() == 1) {
            return alphabet.indexOf(str)+1;
        }
        if(str.length() == 2) {
            return ( alphabet.indexOf(str.substring(1)) + 26*(1+alphabet.indexOf(str.substring(0,1)))+1) ;
        }
        return -1;
    }


    //Is the coordinate a valid position on the defined board size
    public static boolean isValid(int xValue, int yValue)
    {
        if (xValue >= 1 && xValue <= CHESS_SIZE && yValue >= 1 && yValue <= CHESS_SIZE)
        {
            return true;
        } else {
            return false;
        }
    }


    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (o == null || getClass() != o.getClass())
            return false;

        ChessPosition that = (ChessPosition) o;

        return Objects.equals(this.value, that.value);
    }

    @Override
    public int hashCode() {

        return Objects.hash(value);
    }

    //Convert to string
    public String toString()
    {
        String xValue = getAlpha(this.x).toUpperCase();
        String yValue = String.valueOf(this.y);
        return (xValue + yValue);
    }

}
