import java.util.*;


public class Knight {

    //All actions related to this chess piece

    public static ArrayList<ChessPosition> nextMoves(ChessPosition startPosition) {
        //check for next valid moves of knight
        int[] x_axis = {-2, -2, -1, -1, 1, 1, 2, 2};            //include vertical move, max two steps per axis
        int[] y_axis = {-1, 1, -2, 2, -2, 2, -1, 1};            //include vertical move, max two steps per axis

        ArrayList<ChessPosition> validPositions = new ArrayList<ChessPosition>();      //List with valid per board next moves

        for (int i = 0; i < 8; i++) {   //knight can with eight different ways

            if (((startPosition.x + x_axis[i]) >= 1 && (startPosition.x + x_axis[i]) <= x_axis.length) &&
                    ((startPosition.y + y_axis[i]) >= 1 && (startPosition.y + y_axis[i]) <= x_axis.length)) {    //check if it is inside the board

                String coordinates = ChessPosition.getAlpha(startPosition.x + x_axis[i]).toUpperCase() + Integer.toString((startPosition.y + y_axis[i]));
                validPositions.add(new ChessPosition(coordinates));   //Add next move to array
            }
        }

        return validPositions;
    }

    public static int KnightEndingPointReachable(ChessPosition startPosition, ChessPosition endingPosition) {

        //check whether next intended position is unreachable
        //because of max two possible steps per axis: x/y=1/2 or x/y=2
        //sum of coordinates are dividable to 3
        //returns 0 if is reachable, -1 if not, -2 for possible other failures

        int x1 = ChessPosition.getNumber(startPosition.toString().substring(0, 1).toUpperCase());
        int y1 = Integer.parseInt(startPosition.toString().substring(1));
        int x2 = ChessPosition.getNumber(endingPosition.toString().substring(0, 1).toUpperCase());
        int y2 = Integer.parseInt(endingPosition.toString().substring(1));

        if (ChessPosition.isValid(x1, y1) && ChessPosition.isValid(x2, y2)) {

            return (((((x2 - x1) / (y2 - y1)) >= 0.5 && ((x2 - x1) / (y2 - y1)) <= 2) ||
                    (((x2 - x1) / (y2 - y1)) <= -0.5 && ((x2 - x1) / (y2 - y1)) >= -2)) &&
                    (((x2 - x1) + (y2 - y1)) % 3 == 0))
                    ? 0 : -1;

        }
        else {
            return -2;
        }
    }
}

