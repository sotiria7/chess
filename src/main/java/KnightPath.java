import javax.swing.*;
import java.awt.*;
import java.util.*;


public class KnightPath {

    public ChessPosition startPoint;  //start position
    public ChessPosition endPoint;    //end position
    public Knight piece;              //type of piece
    public int NumberOfTurns = 3;     //number of moves
    public String shortestPath = "";  //shortest path

    public KnightPath(ChessPosition start, ChessPosition end, Knight knight)
    {
        startPoint = start;
        endPoint = end;
        piece = knight;
    }

    public void CheckIfDestinationIsUnreachable(){  //in case of moves=1, if ending point is unreachable from starting point there is no solution

        //in case of moves=1, if ending point is unreachable from starting point there is no solution
        if ((piece.KnightEndingPointReachable(startPoint, endPoint)) != 0){
            shortestPath = "There is no solution";
        }

    }

    public void findShortestPathBFS() {  //BFS algorithm for shortest path

        HashMap<ChessPosition, String> rootNodes = new HashMap<ChessPosition, String>(); //unique key is th node, value is the path of its parent and the parents of his parents
                                                                                        // current node inclusive
        Queue<ChessPosition> visitQueue = new LinkedList<ChessPosition>(); //queue nodes to visit

        rootNodes.put(startPoint, startPoint.toString());  //put starting point in Hashmap
        visitQueue.add(startPoint);                        //put the first node to be checked in queue

        while (visitQueue.peek() != null) //check if there are nodes to be visited
        {
            ChessPosition currentNode = visitQueue.poll();  //get the next node from the queue

            if (currentNode.equals(endPoint)) {             //if it is equal to the end then stop searching
                break;

            } else {

                ArrayList<ChessPosition> nextNodes = piece.nextMoves(currentNode); //  for the current node find the next possible vertex
                for (ChessPosition nextPosition : nextNodes) {
                    if (!rootNodes.containsKey(nextPosition))  //check if vertex is already visited with key of Hashmap which is unique
                    {
                        String path = rootNodes.get(currentNode);    //fetch the path to the parent
                        path = path +" "+ nextPosition;              //add the current node
                        rootNodes.put(nextPosition, path);           //store the path to this node as value in Hashmap
                    }

                    visitQueue.add(nextPosition);             //store to queue for further check for its children
                }
            }
        }


            ChessPosition currentNode = endPoint;
            shortestPath = rootNodes.get(currentNode);   //path to end Point is the value of registration with key the name-coordinates of node

            if(shortestPath.trim().split(" ").length!=(NumberOfTurns+1)) //if length of the path elements is not equal to the given moves then there is no solution
                shortestPath = "There is no solution";

    }

    public String getShortestPath() {

        return this.shortestPath;

    }

    public void main(String[] args)
    {
        try {
            if(NumberOfTurns == 1){
                CheckIfDestinationIsUnreachable();
            }else {
                KnightPath knightShortestPath = new KnightPath(new ChessPosition(args[0]), new ChessPosition(args[1]), new Knight());
                knightShortestPath.findShortestPathBFS();  //path calculation
            }

        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage()); //check program
        }

    }

}
