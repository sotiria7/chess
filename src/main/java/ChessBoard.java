import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;


public class ChessBoard extends JFrame{

    private final JPanel gui = new JPanel(new BorderLayout(3, 3));
    private JButton[][] chessSquares = new JButton[8][8];
    private JPanel chessBoard;
    private JPanel panel = new JPanel(new BorderLayout(1, 1));
    private JPanel panelOutput = new JPanel(new BorderLayout(1, 1));
    private final JLabel message = new JLabel("Knight Three Moves!");
    private static final String COLS = "ABCDEFGH";
    private JButton d1;
    private JTextField field1, field2, field3 = new JTextField(10);
    private static int CHESS_SIZE = 8;

    public ChessBoard() {

        initializeGui();

    }

    public final void initializeGui() {
        // set up the main GUI

        //create north Border
        gui.setBorder(new EmptyBorder(5, 5, 5, 5));
        JToolBar tools = new JToolBar();
        tools.setFloatable(false);
        gui.add(tools, BorderLayout.PAGE_START);
        d1 = new JButton("Find Path");
        d1.setName("Find Path");
        tools.add(d1);
        tools.addSeparator();
        JButton d2 = new JButton("Reset");
        tools.add(d2);
        tools.addSeparator();
        tools.add(message);

        //create west Border
        panel = new JPanel(new GridLayout(0, 1));
        field1 = new JTextField(5);
        field1.setBounds(0, 0, 5, 5);
        field1.setName("Coordinate1");
        field2 = new JTextField(15);
        field2.setBounds(50, 50, 5, 5);
        field2.setName("Coordinate2");
        panel.add(new JLabel("Coordinate1:"));
        panel.add(field1);
        panel.add(new JLabel());
        panel.add(new JLabel("Coordinate2:"));
        panel.add(field2);
        panel.add(new JLabel());
        panel.add(new JLabel());
        panel.add(new JLabel());
        panel.add(new JLabel());
        panel.add(new JLabel());
        panel.add(new JLabel());
        panel.add(new JLabel());
        panel.add(new JLabel());
        panel.add(new JLabel());
        panel.add(new JLabel());
        panel.add(new JLabel());
        panel.add(new JLabel());
        gui.add(panel, BorderLayout.WEST);

        //create south border
        panelOutput = new JPanel(new GridLayout(1, 0));
        field3 = new JTextField(15);
        field3.setBounds(50, 50, 20, 5);
        field3.setName("Solution");
        panelOutput.add(new JLabel("Solution"));
        panelOutput.add(field3);
        gui.add(panelOutput, BorderLayout.SOUTH);


        //create chess
        chessBoard = new JPanel(new GridLayout(0, 9));
        chessBoard.setBorder(new LineBorder(Color.BLACK));
        gui.add(chessBoard);

        // create the chess board squares
        Insets buttonMargin = new Insets(0, 0, 0, 0);
        for (int i = 0; i < chessSquares.length; i++) {
            for (int j = 0; j < chessSquares[i].length; j++) {

                JButton b = new JButton();
                b.setMargin(buttonMargin);
                ImageIcon icon = new ImageIcon(new BufferedImage(64, 64, BufferedImage.TYPE_INT_ARGB));
                b.setIcon(icon);

                if ((j % 2 == 1 && i % 2 == 1) || (j % 2 == 0 && i % 2 == 0)) {

                    b.setBackground(Color.WHITE);

                } else {

                    b.setBackground(Color.BLACK);
                }

                chessSquares[j][i] = b;
            }
        }

        //fill the chess board
        chessBoard.add(new JLabel(""));

        // fill the top row
        for (int i = 0; i < 8; i++) {

            chessBoard.add(new JLabel(COLS.substring(i, i + 1), SwingConstants.CENTER));

        }
        // fill the black non-pawn piece row
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                switch (j) {
                    case 0:
                        chessBoard.add(new JLabel("" + (i + 1), SwingConstants.CENTER));
                    default:
                        chessBoard.add(chessSquares[j][i]);
                }
            }
        }

        //Create listener for "Find Path" button
        ActionListener b1Event = new ActionListener() {
           @Override
            public void actionPerformed(ActionEvent e) {
                try {

                    //If clicked then play starts
                    KnightPath GuishortestPath = new KnightPath(new ChessPosition(field1.getText().trim()), new ChessPosition(field2.getText().trim()), new Knight());
                    GuishortestPath.findShortestPathBFS();
                    field3.setText(GuishortestPath.getShortestPath());

                    try {

                        //Each inside node of the path will be colored to CYAN
                        for (String word : GuishortestPath.getShortestPath().split(" ")) {
                            int x = ChessPosition.getNumber(word.substring(0,1).toUpperCase());
                            int y = Integer.parseInt(word.substring(1));
                            chessSquares[x - 1][y - 1].setBackground(Color.CYAN);
                        }

                        //Staring node turned to RED, ending node to GREEN
                        chessSquares[ChessPosition.getNumber(field1.getText().trim().substring(0, 1).toUpperCase()) - 1][Integer.parseInt(field1.getText().trim().substring(1)) - 1].setBackground(Color.RED);
                        chessSquares[ChessPosition.getNumber(field2.getText().trim().substring(0, 1).toUpperCase()) - 1][Integer.parseInt(field2.getText().trim().substring(1)) - 1].setBackground(Color.GREEN);

                    }catch (IllegalArgumentException e1) {
                        throw new IllegalArgumentException("Invalid coordinates of knight");
                    }

                } catch (IllegalArgumentException ex) {

                    field3.setText(ex.getMessage()); //In case of failure, in solution tab, every exception from inside code is printed
                }
            }
        };

        d1.addActionListener(b1Event);  //Add listener to button "Find Path"

        //Create listener for "Reset" button
        //Return to default values
        ActionListener b2Event = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                //initializeGui();
                field1.setText("");
                field2.setText("");
                field3.setText("");
                for (int ii = 0; ii < chessSquares.length; ii++) {
                    for (int jj = 0; jj < chessSquares[ii].length; jj++) {
                        if ((jj % 2 == 1 && ii % 2 == 1) || (jj % 2 == 0 && ii % 2 == 0)) {
                            chessSquares[jj][ii].setBackground(Color.WHITE);
                        } else {
                            chessSquares[jj][ii].setBackground(Color.BLACK);
                        }
                    }
                }
            }
        } ;

        d2.addActionListener(b2Event);    //Add listener to Rest button
    }


    public final JComponent getChessBoard() {

        return chessBoard;
    }


    public final JTextField getField1() {

        return field1;
    }

    public final JTextField getField2() {

        return field2;
    }

    public final JTextField getField3() {

        return field3;
    }

    public final JButton getButton() {

        return d1;
    }

    public final JComponent getGui() {

        return gui;
    }

    public static void main(String[] args) {

        Runnable r = new Runnable() {

            public void run() {
                ChessBoard cb = new ChessBoard();

                JFrame f = new JFrame("Knight in Chess");
                f.add(cb.getGui());
                f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                f.setLocationByPlatform(true);
                f.pack();
                f.setMinimumSize(f.getSize());
                f.setVisible(true);
            }
        };
        SwingUtilities.invokeLater(r);
    }

}