import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;


public class KnightPathTest {

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void shortestPathThreeMoves() throws Exception {

        ChessPosition start = new ChessPosition("E1");
        ChessPosition end = new ChessPosition("E2");
        Knight knight = new Knight();
        KnightPath knightPath = new KnightPath(start, end, knight);
        knightPath.findShortestPathBFS();
        assertEquals("E1 C2 D4 E2", knightPath.getShortestPath().trim());

    }

    @Test
    public void shortestPathMoreMoves() throws Exception {

        ChessPosition start = new ChessPosition("A1");
        ChessPosition end = new ChessPosition("A7");
        Knight knight = new Knight();
        KnightPath knightPath = new KnightPath(start, end, knight);
        knightPath.findShortestPathBFS();
        assertEquals("There is no solution", knightPath.getShortestPath().trim());

    }

    @Test
    public void shortestPathOneMove() throws Exception {

        ChessPosition start = new ChessPosition("A1");
        ChessPosition end = new ChessPosition("B3");
        Knight knight = new Knight();
        KnightPath knightPath = new KnightPath(start, end, knight);
        knightPath.findShortestPathBFS();
        assertEquals("There is no solution", knightPath.getShortestPath().trim());

    }

    @Test
    public void shortestPathExtendMove() throws Exception {

        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Invalid position coordinates");
        ChessPosition start = new ChessPosition("S3");
        ChessPosition end = new ChessPosition("B3");
        Knight knight = new Knight();
        KnightPath knightPath = new KnightPath(start, end, knight);
        knightPath.findShortestPathBFS();
        assertEquals("Invalid position coordinates", knightPath.getShortestPath().trim());

    }

}