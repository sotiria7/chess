import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class KnightTest {

    @Test
    public void validNextMoves() throws Exception {

        ArrayList<ChessPosition> validPositions = new ArrayList<ChessPosition>();
        validPositions.add(new ChessPosition("B3"));
        validPositions.add(new ChessPosition("C2"));
        assertEquals(validPositions, Knight.nextMoves(new ChessPosition("A1")));

    }

    @Test
    public void nonValidNextMoves() throws Exception {

        ArrayList<ChessPosition> validPositions = new ArrayList<ChessPosition>();
        validPositions.add(new ChessPosition("B3"));
        validPositions.add(new ChessPosition("C3"));
        assertNotEquals(validPositions, Knight.nextMoves(new ChessPosition("A1")));

    }

    @Test
    public void knightEndingPointReachable() throws Exception {

        assertEquals(0, Knight.KnightEndingPointReachable(new ChessPosition("A1"), new ChessPosition("C2")));
        assertNotEquals(0, Knight.KnightEndingPointReachable(new ChessPosition("A1"), new ChessPosition("C3")));


    }

}