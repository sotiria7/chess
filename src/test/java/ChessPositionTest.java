import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.HashMap;
import static org.junit.Assert.*;

public class ChessPositionTest {

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void validateCoordinateOverTwoCharactersShouldThrowException() throws Exception {

        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Invalid position coordinates");
        ChessPosition c5 = new ChessPosition("awd12");
        ChessPosition c6 = new ChessPosition("s9");
        ChessPosition c7 = new ChessPosition("a9");

    }

    @Test
    public void validateCoordinateVerticalShouldThrowException() throws Exception {

        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Invalid position coordinates");
        ChessPosition c8 = new ChessPosition("s3");

    }

    @Test
    public void valid() throws Exception {


        ChessPosition c4 = new ChessPosition("C1");

    }

    @Test
    public void ValidCoordinate() throws Exception {

        ChessPosition c1 = new ChessPosition("E5");
        ChessPosition c3 = new ChessPosition("d5");

    }

    @Test
    public void getAlpha() throws Exception {

        assertEquals("E", ChessPosition.getAlpha(5).toUpperCase());
        assertEquals("AA",ChessPosition.getAlpha(27).toUpperCase());

    }

    @Test
    public void getNumber() throws Exception {

        assertEquals(5, ChessPosition.getNumber("E"));
        assertEquals(28, ChessPosition.getNumber("AB"));
        assertEquals(3, ChessPosition.getNumber("C"));

    }

    @Test
    public void isValidCoordinate() throws Exception {

        assertTrue(ChessPosition.isValid(5,4));
        assertFalse(ChessPosition.isValid(10,10));
        assertFalse(ChessPosition.isValid(5,10));
        assertFalse(ChessPosition.isValid(10,5));

    }

    @Test
    public void testEquals()
    {
        ChessPosition position1 = new ChessPosition("E1");
        ChessPosition position2 = new ChessPosition("E1");

        boolean isEqual = position1.equals(position2);

        assertTrue(isEqual);
    }

    @Test
    public void testHashCode()
    {
        ChessPosition position1 = new ChessPosition("E1");
        ChessPosition position2 = new ChessPosition("E1");
        ChessPosition position3 = new ChessPosition("E2");

        int h1 = position1.hashCode();
        int h2 = position2.hashCode();
        int h3 = position3.hashCode();

        assertTrue(h1 == h2);
        assertTrue(h1 != h3);
    }

}